#include "filtro_preto_branco.hpp"
#include "imagem.hpp"
#include <iostream>
#include <math.h>

using namespace std;

Filtro_Preto_Branco::Filtro_Preto_Branco(){}

void Filtro_Preto_Branco::aplica_filtro(Imagem * imagem){
	int escala_cinza;
	for(int i=0; i<imagem->getAltura(); i++){
		for(int j=0; j<imagem->getLargura(); j++){
			escala_cinza = trunc((0.299 * (int)imagem->getPixel_Red(i, j)) + (0.587 * (int)imagem->getPixel_Green(i, j)) + (0.144 * (int)imagem->getPixel_Blue(i, j)));
			if(escala_cinza > imagem->getIntensidade()){
				imagem->setPixel_Red(i, j, (unsigned char)imagem->getIntensidade());
                        	imagem->setPixel_Green(i, j, (unsigned char)imagem->getIntensidade());
                        	imagem->setPixel_Blue(i, j, (unsigned char)imagem->getIntensidade());
			} else{
				imagem->setPixel_Red(i, j, (unsigned char)escala_cinza);
				imagem->setPixel_Green(i, j, (unsigned char)escala_cinza);
				imagem->setPixel_Blue(i, j, (unsigned char)escala_cinza);
			}
		}
	}
}
