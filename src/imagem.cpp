#include "imagem.hpp"
#include <string>
#include <fstream>

using namespace std;

//Imagem::Imagem(){}

Imagem::Imagem(ifstream& arquivo){
   int posicao;
   string linha;
   int conta_comentarios = 0;
   string numero_magico;
   arquivo.seekg(0); 
   int largura, altura, intensidade_pixel;
   while(true){ 
        getline(arquivo, linha);
        if(linha.size() > 50) break;
        if(linha[0]=='#') conta_comentarios++;
   }
   if(conta_comentarios == 0){
        arquivo.seekg(0);
        getline(arquivo, numero_magico);
        setNumero_magico(numero_magico);
        arquivo >> largura;
        setLargura(largura);
        arquivo >> altura;
        setAltura(altura);
        arquivo >> intensidade_pixel;
        setIntensidade(intensidade_pixel);
	posicao = arquivo.tellg();
   } else{
        arquivo.seekg(0);
        getline(arquivo, numero_magico);
        setNumero_magico(numero_magico);
        int contador = 0;
        while(true){ 
                getline(arquivo, linha);
                if(linha[0]=='#'){
                        contador++;
                        if(contador == conta_comentarios){
                                arquivo >> largura;
                                setLargura(largura);
                                arquivo >> altura;
                                setAltura(altura);
                                arquivo >> intensidade_pixel;
                                setIntensidade(intensidade_pixel);
				posicao = arquivo.tellg();
				break;
                        }
                }
        }
   }
   posicao++;
   arquivo.seekg(posicao);
   pixel.resize(getAltura());
   for(int i=0; i < getAltura(); i++){
	pixel[i].resize(getLargura());
   }
   for(int i=0 ; i<getAltura(); i++){
        for(int j=0; j<getLargura(); j++){
                setPixel_Red(i, j, arquivo.get());
                setPixel_Green(i, j, arquivo.get());
                setPixel_Blue(i, j, arquivo.get());
        }
   }
 
}

Imagem::~Imagem(){


}

string Imagem::getNumero_magico(){
   return numero_magico;
}

void Imagem::setNumero_magico(string numero_magico){
   this->numero_magico = numero_magico;
}

int Imagem::getLargura(){
   return largura;
}

void Imagem::setLargura(int largura){
  this->largura = largura;
}

int Imagem::getAltura(){
  return altura;
}

void Imagem::setAltura(int altura){
  this->altura = altura;
}

int Imagem::getIntensidade(){
  return intensidade_pixel;
}

void Imagem::setIntensidade(int intensidade){
  intensidade_pixel = intensidade;
}

void Imagem::setSize_pixel(int largura, int altura){
  pixel.resize(largura);
  for(int i = 0; i < largura; i++){
	pixel[i].resize(altura);	
  }
}

void Imagem::redefinirImagem(ifstream& arquivo){
   int posicao;
   string linha;
   int conta_comentarios = 0;
   string numero_magico;
   arquivo.seekg(0);
   int largura, altura, intensidade_pixel;
   while(true){
        getline(arquivo, linha);
        if(linha.size() > 50) break;
        if(linha[0]=='#') conta_comentarios++;
   }
   if(conta_comentarios == 0){
        arquivo.seekg(0);
        getline(arquivo, numero_magico);
        setNumero_magico(numero_magico);
        arquivo >> largura;
        setLargura(largura);
        arquivo >> altura;
        setAltura(altura);
        arquivo >> intensidade_pixel;
        setIntensidade(intensidade_pixel);
        posicao = arquivo.tellg();
   } else{
        arquivo.seekg(0);
        getline(arquivo, numero_magico);
        setNumero_magico(numero_magico);
        int contador = 0;
        while(true){
                getline(arquivo, linha);
                if(linha[0]=='#'){
                        contador++;
                        if(contador == conta_comentarios){
                                arquivo >> largura;
                                setLargura(largura);
                                arquivo >> altura;
                                setAltura(altura);
                                arquivo >> intensidade_pixel;
                                setIntensidade(intensidade_pixel);
                                posicao = arquivo.tellg();
                                break;
                        }
		 }
        }
   	}
   	posicao++;
   	arquivo.seekg(posicao);
   	pixel.resize(getAltura());
   	for(int i=0; i < getAltura(); i++){
        	pixel[i].resize(getLargura());
   	}
   	for(int i=0 ; i<getAltura(); i++){
        	for(int j=0; j<getLargura(); j++){
                	setPixel_Red(i, j, arquivo.get());
                	setPixel_Green(i, j, arquivo.get());
                	setPixel_Blue(i, j, arquivo.get());
        	}
   	}
}

unsigned char Imagem::getPixel_Red(int linha, int coluna){
  return pixel[linha][coluna].Red;
}

unsigned char Imagem::getPixel_Green(int linha, int coluna){
  return pixel[linha][coluna].Green;
}

unsigned char Imagem::getPixel_Blue(int linha, int coluna){
  return pixel[linha][coluna].Blue;
}

void Imagem::setPixel_Red(int linha, int coluna, unsigned char red){
   pixel[linha][coluna].Red = red;
}

void Imagem::setPixel_Green(int linha, int coluna, unsigned char green){
   pixel[linha][coluna].Green = green;
}

void Imagem::setPixel_Blue(int linha, int coluna, unsigned char blue){
   pixel[linha][coluna].Blue = blue;
}
