#include <iostream>
#include <cstdlib>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include "imagem.hpp"
#include "filtro_negativo.hpp"
#include "filtro_polarizado.hpp"
#include "filtro_preto_branco.hpp"
#include "filtro_media.hpp"

using namespace std;

long int le_teclado_int(int numero_de_opcoes) {

   string input = "";
   long int valor;
   while (true) {
      getline(cin, input);
      stringstream myStream(input);
      if ((myStream >> valor) and (valor >=1 || valor <=numero_de_opcoes))
        break;
      cout << "Inteiro inválido, selecione uma opção: ";
    }
    return valor;
}

void getArquivo(ifstream& arquivo, string nome_arquivo){
	cout << endl << "Forneça o caminho e o nome da imagem inicial: ";
	getline(cin, nome_arquivo);
	arquivo.open(nome_arquivo.c_str());
	if(!arquivo.is_open() or nome_arquivo.find(".ppm") == std::string::npos){
		throw(1);
	}
}

void salvaImagem(Imagem * imagem){
	ofstream imagem_final;
        string opcao;
        cout << "Digite o nome do arquivo final: ";
        getline(cin, opcao);
        opcao += ".ppm";
        imagem_final.open(opcao.c_str(), ios::binary);
        imagem_final << imagem->getNumero_magico() << endl;
        imagem_final << imagem->getLargura() << " " << imagem->getAltura() << endl;
        imagem_final << imagem->getIntensidade() << endl;

        for(int i=0; i<imagem->getAltura(); i++){
                for(int j=0; j<imagem->getLargura(); j++){
                	imagem_final << (char) imagem->getPixel_Red(i, j) << (char) imagem->getPixel_Green(i, j) << (char) imagem->getPixel_Blue(i, j);
        	}
        }
        cout << "Salvando Alterações...." << endl;
        cout << "SUCESSO!!" << endl;
   	imagem_final.close();
}

void pergunta_Salvar_arquivo(Imagem * imagem){
	string escolha;
        getline(cin, escolha);
        if(!escolha.compare("s") || !escolha.compare("S")){
            	salvaImagem(imagem);
      	} else if(!escolha.compare("n") || !escolha.compare("N")){

     	} else{
                do{
                        cout << "Digite uma opção válida 's' ou 'n': ";
                        getline(cin, escolha);
                        if(!escolha.compare("s") || !escolha.compare("S")){
                                salvaImagem(imagem);
                        	break;
                	} else if(!escolha.compare("n") || !escolha.compare("N")) break;
      		}while(1);
   	}
}

int main(int argc, char ** argv){
   system("clear");
   ifstream  arquivo;
   string nome_arquivo;
   
   try{
	getArquivo(arquivo, nome_arquivo);
   } catch(int){
        do{
		cout << endl << "ERRO: arquivo não existe ou o formato é inválido!" << endl;
		cout << "Digite o nome de um arquivo que exista com o formato .ppm: ";
		getline(cin, nome_arquivo);
		nome_arquivo.begin();
		arquivo.close();
		arquivo.open(nome_arquivo.c_str());
	}while(!arquivo.is_open() or nome_arquivo.find(".ppm") == std::string::npos);
   }

   Imagem * imagem_1 = new Imagem(arquivo);
  
   int selecao;
   while(1){
   	cout << endl << endl << "------------------------Menu de Operações--------------------------" << endl;
   	cout << "- (1) Negativo                               (2) Polarizado       -" << endl;
   	cout << "- (3) Preto e Branco                         (4) Media            -" << endl;
   	cout << "- (5) Definir imagem para aplicar filtro     (6) Sair do programa -" << endl;
	cout << "-------------------------------------------------------------------" << endl;

   	do{
		cout << endl << "Selecione uma opção: ";
  		selecao = (int) le_teclado_int(6);
   	}while(selecao < 1 || selecao > 6);

   	if(selecao == 1){
   		Filtro_Negativo filtro;
		filtro.aplica_filtro(imagem_1);
		cout << "Filtro negativo aplicado na imagem " << nome_arquivo << endl;
	} else if(selecao == 2){
		Filtro_Polarizado filtro_2;
		filtro_2.aplica_filtro(imagem_1);
		cout << "Filtro polarizado aplicado na imagem " << nome_arquivo << endl;
	} else if(selecao == 3){
		Filtro_Preto_Branco filtro_3;
		filtro_3.aplica_filtro(imagem_1);
		cout << "Filtro preto e branco aplicado na imagem " << nome_arquivo << endl;
	} else if(selecao == 4){
		int selecao_mascara;
		cout << endl << "----------Tamanho da máscara---------" << endl;
		cout << "-   (1) 3x3    (2) 5x5     (3)7x7   -" << endl;
		cout << "-------------------------------------" << endl;
		do{
			cout << "Selecione um tamanho: ";
			selecao_mascara = (int) le_teclado_int(3);
		}while(selecao_mascara < 1 || selecao_mascara > 3);
		if(selecao_mascara == 1){
			Filtro_Media * filtro_4 = new Filtro_Media(3);
			filtro_4->aplica_filtro(imagem_1);
			cout << "Filtro da media com mascara 3x3 aplicado na imagem " << nome_arquivo << endl;
		} else if(selecao_mascara == 2){
			Filtro_Media * filtro_4 = new Filtro_Media(5);
                	filtro_4->aplica_filtro(imagem_1);
			cout << "Filtro da media com mascara 5x5 aplicado na imagem " << nome_arquivo << endl;
		} else if(selecao_mascara == 3){
			Filtro_Media * filtro_4 = new Filtro_Media(7);
                	filtro_4->aplica_filtro(imagem_1);
			cout << "Filtro da media com mascara 7x7 aplicado na imagem " << nome_arquivo << endl;
		}
	} else if(selecao == 5){
		arquivo.close();
	      cout << endl << "AVISO: Antes de mudar de arquivo, deseja salvar a imagem que estava editando[s/n]? ";
		pergunta_Salvar_arquivo(imagem_1);
		cout << "Forneça o nome e o caminho do novo arquivo: ";
		getline(cin, nome_arquivo);
		arquivo.open(nome_arquivo.c_str());
		do{
			if(arquivo.is_open() and nome_arquivo.find(".ppm") != std::string::npos){
				imagem_1->redefinirImagem(arquivo);
				break;
			} else{
				cout << endl << "ERRO: arquivo não existe ou o formato é inválido!" << endl;
				cout << "Digite o nome de um arquivo que exista com o formato .ppm: ";
				getline(cin, nome_arquivo);
				arquivo.close();
				arquivo.open(nome_arquivo.c_str());
			}
		}while(1);
		system("clear");
	} else if(selecao == 6){
		cout << "AVISO: Antes de sair, deseja salvar as alterações feitas [s/n]? ";
		pergunta_Salvar_arquivo(imagem_1);
		cout << "Fechando programa..." << endl;
		arquivo.close();
		break;
	}
   	
   } 
   return 0;
}
