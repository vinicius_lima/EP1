#include "filtro_polarizado.hpp"
#include "imagem.hpp"
#include <iostream>

using namespace std;

Filtro_Polarizado::Filtro_Polarizado() { }

void Filtro_Polarizado::aplica_filtro(Imagem * imagem){
	for(int i=0; i<imagem->getAltura(); i++){
		for(int j=0; j<imagem->getLargura(); j++){
			if((unsigned int)(imagem->getPixel_Red(i, j)) > (unsigned int)(imagem->getIntensidade()/2)){
				imagem->setPixel_Red(i, j, (unsigned char) imagem->getIntensidade());
			} else{
				imagem->setPixel_Red(i, j, 0);
			}

			if((unsigned int)(imagem->getPixel_Green(i, j)) > (unsigned int)(imagem->getIntensidade()/2)){
                                imagem->setPixel_Green(i, j, (unsigned char) imagem->getIntensidade());
                        } else{
                                imagem->setPixel_Green(i, j, 0);
                        }

			if((unsigned int)(imagem->getPixel_Blue(i, j)) > (unsigned int)(imagem->getIntensidade()/2)){     
                                imagem->setPixel_Blue(i, j, (unsigned char)imagem->getIntensidade());
                        } else{
                                imagem->setPixel_Blue(i, j, 0);
                        }

		}
	}


}
