#include "filtro_negativo.hpp"
#include "imagem.hpp"
#include <iostream>

using namespace std;

Filtro_Negativo::Filtro_Negativo(){

}

void Filtro_Negativo::aplica_filtro(Imagem * imagem){
     unsigned char red, green, blue;
     for(int i=0; i<imagem->getAltura(); i++){
     	for(int j=0; j<imagem->getLargura(); j++){
	 	red =  (unsigned char) (imagem->getIntensidade() - imagem->getPixel_Red(i, j));
	 	green = (unsigned char) (imagem->getIntensidade() - imagem->getPixel_Green(i, j));
         	blue = (unsigned char) (imagem->getIntensidade() - imagem->getPixel_Blue(i, j));
         	imagem->setPixel_Red(i, j, red);
         	imagem->setPixel_Green(i, j, green);
         	imagem->setPixel_Blue(i, j, blue);
     	}
  }
}
