#include "filtro_media.hpp"
#include "imagem.hpp"
#include <iostream>
#include <vector>

using namespace std;

Filtro_Media::Filtro_Media(int tamanho_mascara){
	this->tamanho_mascara = tamanho_mascara;
}

int Filtro_Media::getTamanho_Mascara(){
	return tamanho_mascara;
}

void Filtro_Media::setTamanho_Mascara(int tamanho_mascara){
	this->tamanho_mascara = tamanho_mascara;
}

void Filtro_Media::aplica_filtro(Imagem * imagem){
        vector<vector<Pixel> > matriz_pixels;
        matriz_pixels.resize(imagem->getAltura());
	for(int i=0; i<imagem->getAltura(); i++){
		matriz_pixels[i].resize(imagem->getLargura());
	}

	for(int i=0; i<imagem->getAltura(); i++){
		for(int j=0; j<imagem->getLargura(); j++){
			matriz_pixels[i][j].Red = imagem->getPixel_Red(i, j);
			matriz_pixels[i][j].Green = imagem->getPixel_Green(i, j);
			matriz_pixels[i][j].Blue = imagem->getPixel_Blue(i, j);
		}
	}
	
	int limit = (getTamanho_Mascara()-1)/2;
        unsigned char red, green, blue;
        int contador = 0;
        for(int i=0; i < imagem->getAltura(); i++){
                for(int j=0; j < imagem->getLargura(); j++) {
                int value_red = 0;
                int value_green = 0;
                int value_blue = 0;
                if((i < limit || j < limit) || (i >= (imagem->getAltura() - limit) || j >= (imagem->getLargura() - limit))){
                        for(int x=(i-limit); x <= (i+limit); x++) {
                                for(int y=(j-limit); y<=(j+limit);y++){
                                        if((x >= 0 && x<imagem->getAltura()) && (y >= 0 && y < imagem->getLargura())){
                                                contador++;
                                                value_red += (unsigned int)imagem->getPixel_Red(x, y);
                                                value_green += (unsigned int)imagem->getPixel_Green(x, y);
                                                value_blue += (unsigned int)imagem->getPixel_Blue(x, y);
                                        }
                                }
                        }
                        red = (unsigned char)(value_red/contador);
                        green = (unsigned char)(value_green/contador);
                        blue = (unsigned char)(value_blue/contador);
                        matriz_pixels[i][j].Red = red;
                        matriz_pixels[i][j].Green = green;
                        matriz_pixels[i][j].Blue = blue;
                        contador = 0;
                } else {
                        for(int x=(i-limit); x <= (i+limit); x++) {
                                for(int y=(j-limit); y<=(j+limit);y++){
                                        value_red += (unsigned int)imagem->getPixel_Red(x, y);
                                        value_green += (unsigned int)imagem->getPixel_Green(x, y);
                                        value_blue += (unsigned int)imagem->getPixel_Blue(x, y);
                                }
                        }
                        red = (unsigned char)(value_red/(getTamanho_Mascara() * getTamanho_Mascara()));
                        green = (unsigned char)(value_green/(getTamanho_Mascara() * getTamanho_Mascara()));
                        blue = (unsigned char)(value_blue/(getTamanho_Mascara() * getTamanho_Mascara()));
                        matriz_pixels[i][j].Red = red;
                        matriz_pixels[i][j].Green = green;
                        matriz_pixels[i][j].Blue = blue;
                }

                }
        }
	for(int i=0; i<imagem->getAltura(); i++){
		for(int j=0; j<imagem->getLargura(); j++){
			imagem->setPixel_Red(i, j, matriz_pixels[i][j].Red);
			imagem->setPixel_Green(i, j, matriz_pixels[i][j].Green);
			imagem->setPixel_Blue(i, j, matriz_pixels[i][j].Blue);
		}
	}
}
