# EP1 - OO (UnB - Gama)

Este projeto consiste em um programa em C++ capaz de aplicar filtros em imagens de formato `.ppm`.

## Neste arquivo deve conter as instruções de execução e descrição do projeto.

Instruções de execução:

PASSO 1:
   - Para compilar o programa digite "make" no terminal com o diretório EP1/ selecionado;
   - Após a compilação, digite "make run" no terminal para executa-lo;

PASSO 2:
   - Forneça o caminho e o nome de uma imagem em formato .ppm (Exemplo: ./doc/exemplo.ppm)
	(OBS: caso seja digitado o nome de um arquivo que não exista ou que não seja do formato ".ppm" o programa irá mostrar uma mensagem de erro e pedirá novamente o nome do arquivo);
   - Depois de selecionada a imagem selecione uma opção entre as 6 do "Menu de Operações";

PASSO 3 (Menu de Operações):
   - Opção 1 -> aplica filtro negativo na imagem;
   - Opção 2 -> aplica filtro polarizado na imagem;
   - Opção 3 -> aplica filtro preto e branco na imagem;
   - Opção 4 -> aplica filtro da média. Após selecionada essa opção, o usuário poderá selecionar entre 3 tamanhos de máscara (3x3, 5x5 e 7x7) para aplicar;
   - Opção 5 -> permite ao usuário trocar de imagem para aplicar filtros. Antes de fornecer o nome da nova imagem que deseja trabalhar, um Aviso apareçerá perguntando ao usuário se deseja salvar em um arquivo as alterações feitas.
   - Opção 6 -> permite que o usuário saia do programa. Antes de sair, é perguntado ao usuário se deseja salvar a imagem que ele estava trabalhando.

OBSERVAÇÕES:
   - Na opção 5, ao pedir o nome do novo arquivo que o usuário deseja trabalhar, deve ser fornecido um que exista e possua o formato ".ppm", caso contrário será pedido novamente o nome do arquivo. Além disso, se o usuário desejar salvar as alterações feitas, não é necessário fornecer o formato ".ppm" no nome do arquivo de saída, o programa faz isso automaticamente;
   - Na opção 6, ao fornecer o nome do arquivo onde será salva a imagem, não é necessário fornecer o formato ".ppm" no nome, o programa faz isso automaticamente.
   - IMPORTANTE: É possível aplicar vários filtros numa única imagem. Além disso, o filtro da média consegue ser aplicado na imagem inteira. Divirta-se.
