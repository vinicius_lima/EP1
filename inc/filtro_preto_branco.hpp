#ifndef FILTRO_PRETO_BRANCO_HPP
#define FILTRO_PRETO_BRANCO_HPP

#include "imagem.hpp"
#include "filtro.hpp"
#include <iostream>

using namespace std;

class Filtro_Preto_Branco : public Filtro {
public:
	Filtro_Preto_Branco();
	void aplica_filtro(Imagem * imagem);
};
#endif
