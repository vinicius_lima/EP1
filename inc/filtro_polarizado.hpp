#ifndef FILTRO_POLARIZADO_HPP
#define FILTRO_POLARIZADO_HPP

#include "filtro.hpp"
#include "imagem.hpp"
#include <iostream>

using namespace std;

class Filtro_Polarizado : public Filtro {
public:
	Filtro_Polarizado();
	void aplica_filtro(Imagem * imagem);
};
#endif
