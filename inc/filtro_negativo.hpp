#ifndef FILTRO_NEGATIVO_HPP
#define FILTRO_NEGATIVO_HPP

#include "filtro.hpp"
#include "imagem.hpp"
#include <iostream>

using namespace std;

class Filtro_Negativo : public Filtro {
public:
	Filtro_Negativo();
	void aplica_filtro(Imagem * imagem);
};
#endif
