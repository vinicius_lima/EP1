#ifndef FILTRO_HPP
#define FILTRO_HPP

#include "imagem.hpp"

class Filtro {
public:
	virtual void aplica_filtro(Imagem * imagem_1) = 0;
};
#endif
