#ifndef IMAGEM_HPP
#define IMAGEM_HPP
#include <iostream>
#include <vector>
#include <string>
#include <fstream>

using namespace std;

typedef struct pixels{
   unsigned char Red;
   unsigned char Green;
   unsigned char Blue;
}Pixel;

class Imagem {
private:
   string numero_magico;
   int largura;
   int altura;
   int intensidade_pixel;
   vector<vector<Pixel> > pixel;
   Imagem() {};
public:
   Imagem(ifstream& arquivo);
   ~Imagem();
   string getNumero_magico();
   void setNumero_magico(string numero_magico);
   int getLargura();
   void setLargura(int largura);
   int getAltura();
   void setAltura(int altura);
   int getIntensidade();
   void setIntensidade(int intensidade);
   void setSize_pixel(int largura, int altura);
   void redefinirImagem(ifstream& arquivo);
   unsigned char getPixel_Red(int linha, int coluna);
   unsigned char getPixel_Green(int linha, int coluna);
   unsigned char getPixel_Blue(int linha, int coluna);
   void setPixel_Red(int linha, int coluna, unsigned char Red);
   void setPixel_Green(int linha, int coluna, unsigned char Green);
   void setPixel_Blue(int linha, int coluna, unsigned char Blue);
};
#endif
