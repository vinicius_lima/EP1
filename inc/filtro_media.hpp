#ifndef FILTRO_MEDIA_HPP
#define FILTRO_MEDIA_HPP

#include "filtro.hpp"
#include "imagem.hpp"
#include <iostream>
#include <vector>

using namespace std;

class Filtro_Media : public Filtro {
private:
	int tamanho_mascara;
	Filtro_Media() {};
public:
	Filtro_Media(int tamanho_mascara);
	int getTamanho_Mascara();
	void setTamanho_Mascara(int tamanho_mascara);
	void aplica_filtro(Imagem * imagem);
};
#endif
